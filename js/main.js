function calcularCotizacion() {
    let txtValorAuto = document.getElementById('txtValorAuto').value;
    let txtEnganche = document.getElementById('txtEnganche');
    let txtTotalFinanciar = document.getElementById('txtFinanciar');
    let cmbPlanes = document.getElementById('cmbPlanes').value;
    let pagoMensual = document.getElementById('txtPagoMensual');
    let interes, meses;

    if(txtValorAuto != '') {
        txtEnganche.value = (txtValorAuto*.30);

        if(cmbPlanes == 1) {
            interes = 0.125;
            meses = 12;
        }
        else if(cmbPlanes == 2) {
            interes = 0.172;
            meses = 18;
        }
        else if(cmbPlanes == 3) {
            interes = 0.21;
            meses = 24;
        }
        else if (cmbPlanes == 4) {
            interes = 0.26;
            meses = 36;
        }
        else if(cmbPlanes == 5) {
            interes = 0.45;
            meses = 48;
        }

        txtTotalFinanciar.value = ((txtValorAuto - txtEnganche.value) + ((txtValorAuto - txtEnganche.value)*interes)).toFixed(2);
        pagoMensual.value = (txtTotalFinanciar.value / meses).toFixed(2);
    }
    else {
        alert("Agregar valor del automóvil");
    }
}

function limpiarCampos() {
    let txtValorAuto = document.getElementById('txtValorAuto');
    let txtEnganche = document.getElementById('txtEnganche');
    let txtFinanciar = document.getElementById('txtFinanciar');
    let txtpagoMensual = document.getElementById('txtPagoMensual');

    txtValorAuto.value = '';
    txtEnganche.value = '';
    txtFinanciar.value = '';
    txtpagoMensual.value = '';

}

function soloNumeros() {
    if(event.keyCode < 45 || event.keyCode > 57) {
        event.returnValue = false;
    }
}